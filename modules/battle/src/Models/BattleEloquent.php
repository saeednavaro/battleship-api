<?php

namespace App\Module\Battle\Models;

use Illuminate\Database\Eloquent\Model;
use App\Module\Board\Models\Board;

class BattleEloquent extends Model
{

    protected $table = 'battles';
    protected $guarded = [ 'id' ];
    protected $fillable = [];

    public function boards()
    {
        return $this->hasMany(Board::class , 'battle_id' , 'id');
    }

}

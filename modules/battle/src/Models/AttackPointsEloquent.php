<?php

namespace App\Module\Battle\Models;

use Illuminate\Database\Eloquent\Model;
use App\Module\Board\Models\Board;

class AttackPointsEloquent extends Model
{

    protected $table = 'attack_points';
    protected $guarded = [ 'id' ];
    public $timestamps = false;

    public function board()
    {
        return $this->belongsTo(Board::class);
    }

}

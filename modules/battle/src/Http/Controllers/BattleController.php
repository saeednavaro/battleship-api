<?php

namespace App\Module\Battle\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Module\General\Helpers\ResponseCreator as Response;
use App\Module\General\Helpers\OperationSuccess;
use App\Module\Battle\Http\Requests\createBattleRequest;
use App\Module\Battle\Http\Requests\attackRequest;
use App\Module\Battle\Handlers\BattleHandler;
use App\Module\Board\Models\Board;
use App\Module\Board\Handlers\BoardHandler;

class BattleController extends Controller
{

    protected $module = 'battle';

    public function __construct()
    {
        
    }

    /**
     * create new empty battle 
     * @param createBattleRequest $request
     * @return type
     */
    public function createBattle( createBattleRequest $request )
    {
        $result = BattleHandler::createBattle($request);

        return Response::GenerateResponse(new OperationSuccess() , $result , []);
    }

    public function makeAttack( attackRequest $request , BattleHandler $Handler )
    {


        $Board = Board::find($request->board_id);

        if( $fail_check_res = $Handler->initCheck($Board , $request) )
            return $fail_check_res;


        $mirror_pos = (9 - $request->pick_x) . $request->pick_y;

        $user_id = BoardHandler::getBoardSectionUserID($Board , $mirror_pos);
        $result = $Handler->attack($Board , $mirror_pos , $user_id);

        
        if($BattleState = $Handler->updateBattleState($Board,$user_id))
                return $BattleState;
        
        return Response::GenerateResponse(new OperationSuccess() , $result , [$BattleState ]);
    }

}

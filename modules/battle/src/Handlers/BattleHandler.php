<?php

namespace App\Module\Battle\Handlers;

use App\Module\Board\Models\Board;
use App\Module\Board\Handlers\BoardHandler;
use App\Module\Battle\Models\BattleEloquent;
use App\Module\General\Helpers\ResponseCreator as Response;
use App\Module\General\Helpers\OperationSuccess;
use App\Module\General\Helpers\OperationFailed;
use App\Module\General\Helpers\InvalidOperation;
use App\Module\General\Helpers\NotFoundResource;

class BattleHandler
{

    public function initCheck( $Board , $request )
    {
        if( $Board === null ) {
            return Response::GenerateResponse(new NotFoundResource() , [] , [ 'Board with given id did not found!' ]);
        }

        if( $Board->battle->battle_status == 1 )
            return Response::GenerateResponse(new InvalidOperation() , [] , [ 'The battle game is finished!' ]);

        $coordinates = $request->pick_x . $request->pick_y;

        $user_id = BoardHandler::getBoardSectionUserID($Board , $coordinates);

        if( $user_id != $request->user_id || !BattleHandler::checkAttackIsValid($Board , $coordinates) )
            return Response::GenerateResponse(new OperationFailed() , [] , [ 'Can not pick this position for attack.' ]);

        // check for user's turn for battle
        if( !BattleHandler::checkAttackTurn($Board , $request->user_id) )
            return Response::GenerateResponse(new InvalidOperation() , [] , [ "It's not your turn yet!" ]);
        return;
    }

    public static function checkForBoardCreation( BattleEloquent $Battle )
    {
        if( $Battle->boards->count() >= 1 )
            return FALSE;
        return TRUE;
    }

    public static function createBattle( $request )
    {
        return BattleEloquent::create([ 'battle_status' => 0 ]);
    }

    public static function checkAttackIsValid( Board $Board , $pos ): bool
    {
        $res = $Board->attack_points()->where('picked_position' , $pos)->first();
        if( $res )
            return FALSE;

        return TRUE;
    }

    public static function checkAttackTurn( Board $Board , $attacker_user_id ): bool
    {
        $turn = $Board->battle->turn;

        if( $turn == 1 && $attacker_user_id == $Board->user_id )
            return TRUE;
        elseif( $turn == 2 && $attacker_user_id == $Board->rival_user_id )
            return TRUE;
        return FALSE;
    }

    public function attack( Board $Board , $pos , $user_id )
    {

        if( BattleHandler::hit($Board , $pos) ) {
            $result = BattleHandler::createHitAttackPoint($Board , $user_id , $pos);
        }
        else {
            $result = BattleHandler::createMissAttackPoint($Board , $user_id , $pos);
        }
    }

    /**
     * Determine if a attack is hit or not
     * @param Board $Board
     * @param varchar|int $postion
     * @return bool
     */
    public static function hit( Board $Board , $postion ): bool
    {

        $opponent_user_id = BoardHandler::getBoardSectionUserID($Board , $postion);

        $Ships = $Board->ships()->where('user_id' , $opponent_user_id)->get();

        $hit = false;
        foreach( $Ships as $ship ):
            for( $i = 0; $i < 5; $i++ ):
                if( $ship->coordinates + $i == $postion ):
                    $hit = true;
                endif;
            endfor;
        endforeach;

        return $hit;
    }

    /**
     * create a new attack point model and save 
     * @param Board $Board
     * @param type $user_id
     * @param varchar|int $pos
     * @param bool $status
     * @return AttackPointsEloquent
     */
    public static function createAttackPoint( Board $Board , $user_id , $pos , $status )
    {
        return $Board->attack_points()->create([
                    'user_id' => $user_id ,
                    'picked_position' => $pos ,
                    'status' => $status ? 1 : 0 ,
        ]);
    }

    public static function createHitAttackPoint( Board $Board , $user_id , $pos )
    {
        return self::createAttackPoint($Board , $user_id , $pos , TRUE);
    }

    public static function createMissAttackPoint( Board $Board , $user_id , $pos )
    {
        return self::createAttackPoint($Board , $user_id , $pos , FALSE);
    }

    public static function updateBattleTurn( Board $Board )
    {
        $ToggledTurn = $Board->battle->turn == 1 ? 2 : 1;

        return $Board->battle()->update([ 'turn' => $ToggledTurn ]);
    }

    /**
     * Determine if a battle finished. if yes, return true
     * @param Board $Board
     * @return boolean
     */
    public static function isFinished( Board $Board ): bool
    {

        $TurnUserId = $Board->battle->turn;

        if( $TurnUserId == 1 )
            $opponent_user_id = $Board->rival_user_id;
        else
            $opponent_user_id = $Board->user_id;


        $Ships = $Board->ships()->where('user_id' , $opponent_user_id)->get();

        foreach( $Ships as $ship ):
            for( $i = 0; $i < $ship->size; $i++ ):
                $att_check = $Board->attack_points()->where('picked_position' , $ship->coordinates + $i)->first();
                if( !$att_check ) {
                    return FALSE;
                }
                elseif( $att_check->status == 0 )
                    return FALSE;
            endfor;
        endforeach;
        return TRUE;
    }

    public static function setWinner( Board $Board , $winner_user_id )
    {
        return $Board->battle->update([ 'winner' => $winner_user_id , 'battle_status' => 1 ]);
    }

    public function updateBattleState( Board $Board , $user_id )
    {
        if( $isFinished = BattleHandler::isFinished($Board) ):
            $result = BattleHandler::setWinner($Board , $user_id);
            return Response::GenerateResponse(new OperationSuccess() , [ 'won' => $result ] , [ $isFinished , 'Winner: ' . $user_id ]);
        endif;
        // updating game state.
        BattleHandler::updateBattleTurn($Board);
        return FALSE;
    }

}

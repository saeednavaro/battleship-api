<?php

namespace App\Module\General\Helpers;

use Mockery\Exception;

class ResponseContainer
{
    protected static $StatusCode;
    protected static $Message;

    public static function getStatus()
    {
        return static::$StatusCode;
    }

    public static function getMessage()
    {
        return static::$Message;
    }
}

class Unauthenticated extends ResponseContainer
{
    protected static $StatusCode = 401;
    protected static $Message = "UNAUTHENTICATED";
}

class NotFoundResource extends ResponseContainer
{
    protected static $StatusCode = 404;
    protected static $Message = "Resource_NotFound";
}

class InvalidOperation extends ResponseContainer
{
    protected static $StatusCode = 406;
    protected static $Message = "INVALID_OPERATION";
}

class InvalidParameters extends ResponseContainer
{
    protected static $StatusCode = 400;
    protected static $Message = "INVALID_PARAMETERS";
}

class AccessDenied extends ResponseContainer
{
    protected static $StatusCode = 403;
    protected static $Message = "ACCESS_DENIED";
}

class OperationFailed extends ResponseContainer
{
    protected static $StatusCode = 405;
    protected static $Message = "OPERATION_FAILED";
}

class OperationSuccess extends ResponseContainer
{
    protected static $StatusCode = 200;
    protected static $Message = "OPERATION_SUCCEED";
}

class ResponseCreator
{
    /**
     * Generate a response object.
     *
     * @param ResponseContainer $container determinate status and
     * @param array $result this is data which must be represented to user
     * @param array $extra this is a array which contains extra messages which are
     * generated while validation inputs or processing the request.
     *
     * @return \Illuminate\Http\Response
     */
    public static function GenerateResponse(ResponseContainer $container, $result, $extra)
    {
        if (self::is_associative((array)$extra)) {
            throw new Exception("Extra could not be an associative array!");
        }

        if (!self::is_associative((array)$result) && !empty((array)$result)) {
            throw new Exception("Result must be an associative array!");
        }
        $body = array(
            "message" => $container::getMessage(),
            "extra" => $extra,
            "result" => (object)$result
        );

        //$response = new \Illuminate\Http\Response();
        return response()->json($body)->setStatusCode($container::getStatus());

    }

    private static function is_associative($param)
    {
        $keys = array_keys($param);
        return array_keys($keys) !== $keys;
    }
}


<?php

namespace App\Module\Ship\Models;

use Illuminate\Database\Eloquent\Model;
use App\Module\Board\Models\Board;

class Ship extends Model
{

    protected $table = 'ships';
    protected $guarded = [ 'id' ];

    public function board()
    {
        return $this->belongsTo(Board::class);
    }

}

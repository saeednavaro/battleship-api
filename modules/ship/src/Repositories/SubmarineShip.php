<?php

namespace App\Module\Ship\Repositories;

use App\Module\Ship\Repositories\ShipInterface;

class SubmarineShip implements ShipInterface
{

    private $size;
    private $coordinates;

    public function __construct()
    {
        $this->size = 3;
    }

    public function setCoordinates( $data )
    {
        $this->coordinates = $data;
    }

    public function getCoordinates()
    {
        return $this->coordinates;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function isHited()
    {
        
    }

    public function isSunked()
    {
        
    }

}

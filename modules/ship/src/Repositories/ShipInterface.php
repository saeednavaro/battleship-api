<?php

namespace App\Module\Ship\Repositories;

interface ShipInterface
{

    public function setCoordinates( $data );

    public function getSize();

    public function getCoordinates();

    public function isHited();

    public function isSunked();
}

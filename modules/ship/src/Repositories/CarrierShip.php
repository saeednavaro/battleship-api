<?php

namespace App\Module\Ship\Repositories;

use App\Module\Ship\Repositories\ShipInterface;
use App\Module\Ship\Models\Ship;

class CarrierShip implements ShipInterface
{

    private $size;
    private $coordinates;

    public function __construct()
    {
        $this->size = 5;
    }

    public function setCoordinates( $data )
    {
        return $this->coordinates = $data;
    }

    public function getCoordinates()
    {
        return $this->coordinates;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function isHited()
    {
        
    }

    public function isSunked()
    {
        
    }

    public function save()
    {
        
    }

}

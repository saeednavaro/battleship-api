<?php

namespace App\Module\Ship\Repositories;

use App\Module\Ship\Repositories\ShipInterface;
use App\Module\Ship\Repositories\CarrierShip;
use App\Module\Ship\Repositories\BattleShip;
use App\Module\Ship\Repositories\SubmarineShip;
use App\Module\Ship\Repositories\CruiserShip;
use App\Module\Ship\Repositories\PatrolShip;

class ShipFactory
{

    public static function create( $size = 1 , $coordinates )
    {

        switch( $size )
        {
            case 5:
            case 0:
                $Ship = new CarrierShip();
                break;
            case 4:
                $Ship = new BattleShip();
                break;
            case 3:
                $Ship = new SubmarineShip();
                break;
            case 2:
                $Ship = new CruiserShip();
                break;
            case 1:
                $Ship = new PatrolShip();
                break;
        }
        $Ship->setCoordinates($coordinates);

        return $Ship;
    }

}

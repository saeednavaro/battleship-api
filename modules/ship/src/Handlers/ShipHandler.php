<?php

namespace App\Module\Ship\Handlers;

use App\Module\Ship\Repositories\ShipInterface;
use App\Module\Board\Models\Board;
use Illuminate\Support\Facades\Config;

class ShipHandler
{

    /**
     * 
     * @param ShipInterface $Ship
     * @param Board $Board
     * @return ShipInterface $Ship
     */
    public static function createShip( ShipInterface $Ship , Board $Board )
    {

        // ship assigned for ?
        $user_id = $Ship->getCoordinates() <= Config::get('constants.board_sep_point') ? $Board->user_id : $Board->rival_user_id;

        $data = array(
            'user_id' => $user_id ,
            'coordinates' => $Ship->getCoordinates() ,
            'size' => $Ship->getSize() ,
            'name' => class_basename($Ship) ,
        );

        return $Board->ships()->create($data);
    }

}

<?php

function alphabet_pos( $alphabet ): int
{
    return ord(strtoupper($alphabet)) - ord('A') + 1;
}

function rand_chr( $first_char = 'A' , $last_char = 'Z' , $limit = 90 )
{
    $position = rand(ord($first_char) , ord($last_char) - $limit);

    return chr($position);
}

function rand_pos_on_board( $size , $shipPlaces )
{
    $first_user_section = 0;
    $second_user_section = 0;

    // the count of ships for each player
    foreach( $shipPlaces as  $val ):
        if( $val <= Config::get('constants.board_sep_point') )
            $first_user_section += 1;
        else
            $second_user_section += 1;
    endforeach;

    // each user can has up to 5 ships on board.
    if( $first_user_section >= 5 ) {
        $X = rand(5 , 9);
    }
    elseif( $second_user_section >= 5 ) {
        $X = rand(0 , 4);
    }
    else {
        $X = rand(0 , 9);
    }
    $Y = rand(0 , 9 - $size);
        
    return $X . $Y;
}

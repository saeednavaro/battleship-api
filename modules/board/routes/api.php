<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your Module. These routes
  | are loaded by the module's RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

// if we want authentication
//Route::prefix('v1')->middleware('auth:api')->group(function () {
Route::prefix('v1')->group(function () {
    Route::post('/board' , 'BoardController@createBoard');
    Route::post('/board/ship' , 'BoardController@createBoardWithRandomShips');
    Route::post('/board/{board_id}/ship' , 'BoardController@placeShip');
});

<?php

namespace App\Module\Board\Handlers;

use App\Module\Board\Models\Board;
use App\Module\Ship\Repositories\ShipFactory;
use App\Module\Battle\Models\BattleEloquent;
use App\Module\Battle\Handlers\BattleHandler;
use Illuminate\Support\Facades\Config;
use App\Module\Board\Http\Requests\placeShipRequest;
use App\Module\General\Helpers\ResponseCreator as Response;
use App\Module\General\Helpers\InvalidOperation;
use App\Module\General\Helpers\AccessDenied;
use App\Module\General\Helpers\NotFoundResource;

class BoardHandler
{

    private $ShipsPos = array();

    public function setPos( $i , $xy )
    {
        return $this->ShipsPos[$i] = $xy;
    }

    public function getAllShipsPos(): array
    {
        return $this->ShipsPos;
    }

    public function getPosByCoordinates( $xy )
    {
        foreach( $this->ShipsPos as $key => $val ):
            if( $val == $xy )
                return $this->ShipsPos[$key];
        endforeach;
        return FALSE;
    }

    public function hasShipOnPos( $i = FALSE , $pos ): bool
    {
        foreach( $this->ShipsPos as $key => $val ):
            if( $i !== FALSE ) {
                if( $key != $i && $pos == $val )
                    return TRUE;
            }else {
                if( $pos == $val )
                    return TRUE;
            }
        endforeach;
        return FALSE;
    }

    public static function createBoard( $request , BattleEloquent $Battle )
    {
        $user_id = $request->user_id;

        $rival_user_id = $request->rival_user_id;

        if( BattleHandler::checkForBoardCreation($Battle) )
            $result = $Battle->boards()->create([ 'user_id' => $user_id , 'rival_user_id' => $rival_user_id ]);
        else
            return FALSE;

        return $result;
    }

    public function placeCheck( $Board , placeShipRequest $request )
    {
        
        if( !$Board )
            return Response::GenerateResponse(new NotFoundResource() , [] , [ 'Board with given id did not found' ]);
        elseif( $Board->user_id != $request->user_id )
            return Response::GenerateResponse(new AccessDenied() , [] , [ 'Access denied for this board.' ]);

        // check that given position is free
        if( !BoardHandler::checkPosPlaceable($Board , $request->ship_size , $request->position_x , $request->position_y) )
            return Response::GenerateResponse(new InvalidOperation() , [] , [ 'Can not place on this position.' ]);
        
        return;
    }

    public function createRandomly( $ship_counts = NULL )
    {
        if( !$ship_counts )
            $ship_counts = rand(1 , 10);
            
        return $this->randomPositionOnBoard($ship_counts);
    }

    public function randomPositionOnBoard( $ship_counts )
    {

        for( $i = 1; $i <= $ship_counts; $i++ )
        {
            $randomSize = (rand(100 , 999)) % 5;

            do
            {
                $space = rand_pos_on_board($randomSize , $this->getAllShipsPos());

                $this->setPos($i , $space);
            } while( $this->hasShipOnPos($space , $i) );
            $Ships[] = ShipFactory::create($randomSize , $this->getPosByCoordinates($space));
        }
        return $Ships;
    }

    public static function checkPosPlaceable( Board $Board , $ship_size , $x , $y ): bool
    {

        $user_id = BoardHandler::getBoardSectionUserID($Board , $x . $y);
        // each user can place up to 5 ships
        if( $Board->ships()->where('user_id' , $user_id)->get()->count() > 4 )
            return FALSE;

        foreach( $Board->ships as $ship ):

            for( $i = 0; $i < $ship_size; $i++ )
            {
                if( $x . ($y + $i) == $ship->coordinates || $x + $i > 9 )
                    return FALSE;
            }
        endforeach;
        return TRUE;
    }

    /**
     * detect the user's id for the given coordinates on a board
     * @param Board $Board
     * @param string|int $coordinates
     * @return int user_id
     */
    public static function getBoardSectionUserID( Board $Board , $coordinates ): int
    {
        if( $coordinates < Config::get('constants.board_sep_point') )
            $user_id = $Board->user_id;
        else
            $user_id = $Board->rival_user_id;
        return $user_id;
    }

}

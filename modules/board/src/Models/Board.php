<?php

namespace App\Module\Board\Models;

use Illuminate\Database\Eloquent\Model;
use App\Module\Ship\Models\Ship;
use App\Module\Battle\Models\BattleEloquent;
use App\Module\Battle\Models\AttackPointsEloquent;

class Board extends Model
{

    protected $table = 'boards';
    protected $guarded = [ 'id' ];

    public function ships()
    {
        return $this->hasMany(Ship::class);
    }

    public function battle()
    {
        return $this->belongsTo(BattleEloquent::class , 'battle_id' , 'id');
    }

    public function attack_points()
    {
        return $this->hasMany(AttackPointsEloquent::class , 'board_id' , 'id');
    }

}

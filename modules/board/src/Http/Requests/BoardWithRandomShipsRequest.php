<?php

namespace App\Module\Board\Http\Requests;

use Illuminate\Http\JsonResponse;
use App\Module\General\Helpers\ResponseCreator as Response;
use App\Module\General\Helpers\InvalidParameters;
use Illuminate\Foundation\Http\FormRequest;

class BoardWithRandomShipsRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;//Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return[
            'user_id' => 'required|integer',
            'rival_user_id' => 'required|integer',
            'battle_id' => 'required|integer',
            'ship_counts' => 'integer|nullable|max:10' ,
        ];
    }

    /**
     * Preparing suitable JSON response for validation errors 
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function response( array $errors )
    {
        foreach( $errors as $val ):
            $errorResponse[] = $val;
        endforeach;
        return Response::GenerateResponse(new InvalidParameters() , new \stdClass() , $errorResponse);
    }

}

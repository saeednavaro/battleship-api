<?php

namespace App\Module\Board\Http\Requests;

use Illuminate\Http\JsonResponse;
use App\Module\General\Helpers\ResponseCreator as Response;
use App\Module\General\Helpers\InvalidParameters;
use Illuminate\Foundation\Http\FormRequest;

class placeShipRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return[
            'board_id' => 'required|integer' ,
            'user_id' => 'required|integer' ,
            'position_x' => 'required|numeric|max:9' ,
            'position_y' => 'required|numeric|max:9' ,
            'ship_size' => 'required|integer|max:5' ,
        ];
    }

    /**
     * Preparing suitable JSON response for validation errors 
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function response( array $errors )
    {
        foreach( $errors as $val ):
            $errorResponse[] = $val;
        endforeach;
        return Response::GenerateResponse(new InvalidParameters() , new \stdClass() , $errorResponse);
    }

}

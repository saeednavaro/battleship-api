<?php

namespace App\Module\Board\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Module\General\Helpers\ResponseCreator as Response;
use App\Module\General\Helpers\OperationSuccess;
use App\Module\General\Helpers\OperationFailed;
use App\Module\General\Helpers\InvalidOperation;
use App\Module\General\Helpers\NotFoundResource;
use App\Module\Board\Http\Requests\createBoardRequest;
use App\Module\Board\Http\Requests\BoardWithRandomShipsRequest;
use App\Module\Board\Http\Requests\placeShipRequest;
use App\Module\Board\Handlers\BoardHandler;
use App\Module\Board\Models\Board;
use App\Module\Ship\Handlers\ShipHandler;
use App\Module\Ship\Repositories\ShipFactory;
use App\Module\Battle\Models\BattleEloquent;

class BoardController extends Controller
{

    protected $module = 'board';

    public function __construct()
    {
        
    }

    /**
     * create an empty default board 
     * @param createBoardRequest $request
     * @return type
     */
    public function createBoard( createBoardRequest $request )
    {

        $Battle = BattleEloquent::find($request->battle_id);

        if( !$Battle )
            return Response::GenerateResponse(new OperationFailed() , [] , [ 'Battle with given id does not exist.' ]);
        $result = BoardHandler::createBoard($request , $Battle);

        if( !$result )
            return Response::GenerateResponse(new InvalidOperation() , [] , [ 'Can not create new board on this battle' ]);
        else
            return Response::GenerateResponse(new OperationSuccess() , $result , []);
    }

    /**
     * 
     * create a default board with random ships.
     * @param BoardWithRandomShipsRequest $request
     * @return type
     */
    public function createBoardWithRandomShips( BoardWithRandomShipsRequest $request , BoardHandler $Handler )
    {
        $Battle = BattleEloquent::find($request->battle_id);

        if( !$Battle )
            return Response::GenerateResponse(new NotFoundResource() , [] , [ 'Battle with given id does not exist.' ]);

        $board = BoardHandler::createBoard($request , $Battle);
        // BOARD OVERFLOW ON BOARD
        if( !$board )
            return Response::GenerateResponse(new InvalidOperation() , [] , [ 'Can not create new board on this battle' ]);


        // CREATING RANDOM SHIPS ON RANDOM POSITIONS
        $Ships = $Handler->createRandomly($request->ship_counts);

        // CREATE AND SAVE SHIPS' MODEL TO DATABASE
        foreach( $Ships as $ship )
        {
            $result[class_basename($ship)] = ShipHandler::createShip($ship , $board);
        }

        return Response::GenerateResponse(new OperationSuccess() , $result , []);
    }

    /**
     * Place a ship on the given board
     * @param placeShipRequest $request
     * @return type
     */
    public function placeShip( placeShipRequest $request, BoardHandler $Handler )
    {
        $Board = Board::find($request->board_id);

        if($check_failed = $Handler->placeCheck($Board , $request))
                return $check_failed;
        
        $requestPos = $request->position_x . $request->position_y;

        $ship = ShipFactory::create($request->ship_size , $requestPos);

        // save to db
        $result[class_basename($ship)] = ShipHandler::createShip($ship , $Board);

        return Response::GenerateResponse(new OperationSuccess() , $result , []);
    }

}

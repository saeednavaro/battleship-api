<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ships' , function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('coordinates' , 2);
            $table->char('name' , 250)->nullable();
            $table->tinyInteger('size')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('board_id');
            $table->foreign('board_id')
                    ->references('id')
                    ->on('boards')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('ships');
    }

}

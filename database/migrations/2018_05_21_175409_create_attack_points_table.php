<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttackPointsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attack_points' , function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('picked_position' , 2);
            $table->tinyInteger('status')->default(0);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('board_id');
            $table->foreign('board_id')
                    ->references('id')
                    ->on('boards')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attack_points');
    }

}

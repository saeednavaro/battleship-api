<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battles' , function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('battle_status')->default(0);
            $table->tinyInteger('winner')->default(0);
            $table->tinyInteger('turn')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battles');
    }

}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGameIdInBoardtable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boards' , function($table) {
            $table->unsignedBigInteger('battle_id');
            $table->foreign('battle_id')
                    ->references('id')
                    ->on('games')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('boards' , function($table) {
            $table->dropColumn('battle_id');
        });
    }

}

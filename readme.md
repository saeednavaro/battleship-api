# Welcome to Battleship!
Hi! this project is about the battleship game API 
## Intro
The battleship API implemented with Laravel 5.6. the project has been developed under a **modular** structure.
with this API you can:
-   Create an empty board.

-   Create a random board for battle with the ships already placed.

-   Place a ship on the board.
    
-   Make an attacking move, determining if it is a hit or a miss and updating the game state.
    
-   Determine the current state of the game, finished (and who won), in play.

In battleship, a board consists of a grid of 10 x 10, labeled vertically with the numbers 1 to 10 (from top to bottom) and labeled 
horizontally with the letters a to j (from left to right).

A player knows:

 - where their own ships are

 - where their own previous attacking moves have been made, and their result

 - where their opponent's previous attacking moves have been made and their result

To play the game, each player, plays an attacking move in turn, the
result of this may be a "hit" if the square is occupied by an
opponent's ship, or a "miss" if the square is not. A player wins when
they have "hit" every square occupied by their opponents ships.

Each player should start the game with 5 ships, laid out in
non-overlapping positions on their own board:

| Ship       | Size (in squares) |
| ---------- | ----------------- |
| Carrier    | 1 x 5             |
| Battleship | 1 x 4             |
| Submarine  | 1 x 3             |
| Cruiser    | 1 x 2             |
| Patrol     | 1 x 1             |


It's nice to have a look at the entities' flow chart before start!
```mermaid
graph LR
A(Battle) -- has one --> B(Board)
B --> D((Player 1))
B --> C((Player 2))
E --> D
E --> C
B --> E[Ships]
```
before start using the APIs, please migrate the database tables with `php artisan migrate` command in command-line. also don't forget to run `composer install` at the first!

## API
#### create a new battle game
##### Endpoint: `POST: api/v1/battle`
##### Parameters:  
#### create a new board for battle
##### Endpoint: `POST: api/v1/board`
##### Parameters: `user_id` - `rival_user_id` - `battle_id`
#### create a new random board for battle with random ships
##### Endpoint: `POST: api/v1/board/ship`
##### Parameters: `user_id` - `rival_user_id` - `battle_id` - `ship_counts` [optional]
						 
> **Note:** The **`ship_counts`** parameter is optional. so if you do not pass any values for this parameter, it will be generated randomly for both players.

#### place a new ship on a board
##### Endpoint: `POST: api/v1/board/{board_id}/ship`
##### Parameters: `user_id` - `board_id` -  `position_x` - `position_y` - `ship_size`[integer]
> **Note:** **`ship_size`** is a integer parameter so you just need to pass the width of ships as we know all the ships' height equivalent to 1.

#### make an attack and get the result of attack
##### Endpoint: `POST: api/v1/battle/attack`
##### Parameters: `user_id` - `board_id` - `pick_x` - `pick_y`
						 

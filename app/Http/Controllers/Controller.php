<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	protected $statusCode = 200;

	public function setStatusCode( $code ) {
		$this->statusCode = $code;
		return $this; //we should return $this to use this method in a chain of methods.
	}

	public function getStatusCode() {
		return $this->statusCode;
	}

	public function resultNotFound( $message = 'Resource not found!' ) {
		return $this->setStatusCode( 404 )->respondError( $message );
	}

	/**
	 * @param string $message
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function notAuthenticated( $message = 'not authenticated!',$extra='' ) {
			return $this->setStatusCode( 401 )->respondError( $message,$extra );
	}

	public function accessDenied( $message = 'access denied!',$extra='' ) {
			return $this->setStatusCode( 403 )->respondError( $message,$extra );
	}

	public function respondError( $message = 'Error', $outflag = false, $headers = [] ) {
		return response()->json( [
			'outFlag' => $outflag,
			'message' => $message
		], $this->getStatusCode(), $headers );
	}

	public function respond( $extra = '', $message = '', $data = '', $headers = [] ) {
		return response()->json( [
			'extra'   => $extra,
			'message' => $message,
			'Result'  => $data
		], $this->getStatusCode(), $headers );
	}
}

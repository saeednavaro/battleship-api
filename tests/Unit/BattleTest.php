<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Module\Battle\Models\BattleEloquent;
use App\Module\Board\Handlers\BoardHandler;
use App\Module\Board\Models\Board;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;

class BattleTest extends TestCase
{
//    use RefreshDatabase;

    /**
     * Test new battle api
     *
     * @return void
     */
    public function testCreateBattle()
    {
        $response = $this->withHeaders([
                ])->json('POST' , '/api/v1/battle');

        $response
                ->assertStatus(200)
                ->assertJson([
                    'message' => 'OPERATION_SUCCEED' ,
        ]);
    }

    public function testCheckCreateBoard()
    {
        $BattleEloquent = Mockery::mock(BattleEloquent::class)->makePartial();

        $battleHandler = new \App\Module\Battle\Handlers\BattleHandler();

        $this->assertTrue($battleHandler->checkForBoardCreation($BattleEloquent));
    }

//    public function testMakeAttack()
//    {

//        $Board = factory(Board::class)->create();
//
//        $response = $this->json('POST' , 'api/v1/battle/attack' , [ 'board_id' => 1 ,
//            'user_id' => 1 ,
//            'pick_x' => 4 ,
//            'pick_y' => 2 ]);
//
//        $response
//                ->assertJson([
//                    'message' => 'OPERATION_SUCCEED' ,
//        ]);
//    }

}

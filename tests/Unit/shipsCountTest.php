<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Module\Board\Handlers\BoardHandler;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;

class shipsCountTest extends TestCase
{

//    use RefreshDatabase;

    public function tearDown()
    {
        Mockery::close();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testShipsCount()
    {
        $BoardHandler = new BoardHandler();

        $ships = $BoardHandler->createRandomly(5);
        $this->assertEquals(5 , count($ships));

        $ships = $BoardHandler->createRandomly(3);
        $this->assertEquals(3 , count($ships));

        $ships = $BoardHandler->createRandomly(4);
        $this->assertEquals(4 , count($ships));

        $randCount = rand(1 , 5);
        $ships = $BoardHandler->createRandomly($randCount);
        $this->assertEquals($randCount , count($ships));
    }


}
